# thont1 microservice exercise

## Running app
```
docker-composer build
docker-composer up
```

## Creating DB
```
docker-compose exec --user node app yarn run migration
```

## Seeding testing data, 
 - I create 5 user in seeder with uuid: user_1, user_2, user_3, user_4, user_5.
 - I use uuid like above for ease to remmeber to test
```
docker-compose exec --user node app yarn run seed
```

## Running unit test
```
docker-compose exec --user node app yarn run test
```


## Opening Swagger
http://localhost:3000/api-docs

- You can test API on it
