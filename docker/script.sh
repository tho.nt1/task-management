set -e
  export HOST=0.0.0.0
  cd /app
  yarn install
  PORT=3000 yarn run start
exec "$@"
