
exports.CONST_FOLLOWING = 'following';
exports.CONST_UN_FOLLOWING = 'un_following';
exports.STATUS_COMPLETED = 'completed';
exports.STATUS_IN_COMPLETE = 'in_complete';

exports.CODE_SUCCESS = 0;
exports.CODE_ERR = 1;
exports.CODE_ERR_MISSING_INPUT = 2;
exports.CODE_ERR_NOT_EXIST_DATA = 3;