const express = require('express')
const taskRouter = require('./task');
const adminRouter = require('./admin');
const userRouter = require('./user');

const rootRouter = express.Router();
rootRouter.use('/api/tasks', taskRouter);
rootRouter.use('/api/admins', adminRouter);
rootRouter.use('/api/users', userRouter);

module.exports = rootRouter;
