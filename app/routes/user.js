const express = require('express')
const router = express.Router();

const user_controllers = require('../controllers/userController');


/**
   * @swagger
   * /api/users/search_mark_tasks:
   *   get:
   *     description: Search all completed or in_complete task of assignee (who is responsible to this task).
   *     tags: [Users]
   *     produces:
   *       - application/json
   *       - application/xml
   *     parameters:
   *       - name: uuid_assignee
   *         description: The uuid of user who is responsible to tasks.
   *         in: query
   *         required: true
   *         type: string
   *       - name: status
   *         description: It is completed or in_complete mark.
   *         in: query
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: No error on server, check respon to see the result
   */
router.get('/search_mark_tasks', user_controllers.searchMarkTasks);

/**
   * @swagger
   * /api/users/search_following_tasks:
   *   get:
   *     description: Search all task that this user follow.
   *     tags: [Users]
   *     produces:
   *       - application/json
   *       - application/xml
   *     parameters:
   *       - name: uuid
   *         description: The uuid of user .
   *         in: query
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: No error on server, check respon to see the result
   */
router.get('/search_following_tasks', user_controllers.searchFollowingTasks);

/**
   * @swagger
   * /api/users/delete_all_data_of_user:
   *   delete:
   *     description: Delete all data belonging to a particular user.
   *     tags: [Admins]
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: uuid
   *         description: The uuid of user we want to delete data.
   *         in: query
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: No error on server, check respon to see the result
   */
router.delete('/delete_all_data_of_user', user_controllers.deleteAllDataOfUser);

module.exports = router;
