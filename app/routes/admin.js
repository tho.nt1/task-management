const express = require('express')
const router = express.Router();

const admin_controllers = require('../controllers/adminController');

router.get('/', admin_controllers.index);
router.get('/get_tasks', admin_controllers.getTasks);

module.exports = router;
