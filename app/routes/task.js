const express = require('express')
const router = express.Router();

const task_controllers = require('../controllers/taskController');

/**
   * @swagger
   * /api/tasks:
   *   post:
   *     description: Create task.
   *     tags: [Tasks]
   *     produces:
   *       - application/json
   *       - application/xml
   *     parameters:
   *       - name: uuid
   *         description: The uuid of creator.
   *         in: query
   *         required: true
   *         type: string
   *       - name: uuid_assignee
   *         description: The uuid of assignee.
   *         in: query
   *         required: true
   *         type: string
   *       - name: content
   *         description: The content of task.
   *         in: query
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: No error on server, check respon to see the result
   */
router.post('/', task_controllers.create);

/**
   * @swagger
   * /api/tasks/{task_id}:
   *   put:
   *     operationId: update Tas
   *     summary: Id of task need to update.
   *     description: Update task.
   *     tags: [Tasks]
   *     produces:
   *       - application/json
   *       - application/xml
   *     parameters:
   *       - name: task_id
   *         description: Id of task need to update.
   *         in: path
   *         required: true
   *         type: string
   *       - name: content
   *         description: Content of task.
   *         in: query
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: No error on server, check respon to see the result
   */
router.put('/:id', task_controllers.update);
router.delete('/:id', task_controllers.delete);
router.put('/mark/:id', task_controllers.mark);
router.put('/following/:id', task_controllers.following);

module.exports = router;
