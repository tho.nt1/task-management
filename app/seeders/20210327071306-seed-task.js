'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    return queryInterface.bulkInsert('Tasks', [
      {
        user_id: 1,
        assignee: 2,
        status: 'in_complete',
        content: 'content 1',
        createdAt: new Date().toDateString(),
        updatedAt: new Date().toDateString()
      },
      {
        user_id: 1,
        assignee: 3,
        status: 'in_complete',
        content: 'content 2',
        createdAt: new Date().toDateString(),
        updatedAt: new Date().toDateString()
      },
      {
        user_id: 1,
        assignee: 4,
        status: 'in_complete',
        content: 'content 3',
        createdAt: new Date().toDateString(),
        updatedAt: new Date().toDateString()
      },
      {
        user_id: 2,
        assignee: 3,
        status: 'in_complete',
        content: 'content 4',
        createdAt: new Date().toDateString(),
        updatedAt: new Date().toDateString()
      },
      {
        user_id: 2,
        assignee: 4,
        status: 'in_complete',
        content: 'content 5',
        createdAt: new Date().toDateString(),
        updatedAt: new Date().toDateString()
      },
      {
        user_id: 2,
        assignee: 5,
        status: 'in_complete',
        content: 'content 6',
        createdAt: new Date().toDateString(),
        updatedAt: new Date().toDateString()
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
