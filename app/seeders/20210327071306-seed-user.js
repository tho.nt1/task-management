'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

     return queryInterface.bulkInsert('Users', [
      {
        uuid: 'user_1',
        createdAt: new Date().toDateString(),
        updatedAt: new Date().toDateString()
      },
      {
        uuid: 'user_2',
        createdAt: new Date().toDateString(),
        updatedAt: new Date().toDateString()
      },
      {
        uuid: 'user_3',
        createdAt: new Date().toDateString(),
        updatedAt: new Date().toDateString()
      },
      {
        uuid: 'user_4',
        createdAt: new Date().toDateString(),
        updatedAt: new Date().toDateString()
      },
      {
        uuid: 'user_5',
        createdAt: new Date().toDateString(),
        updatedAt: new Date().toDateString()
      },
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

     await queryInterface.bulkDelete('People', null, {});
  }
};
