'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

     return queryInterface.bulkInsert('TaskUsers', [
        {
          user_id: 2,
          task_id: 1,
          following: 0,
          createdAt: new Date().toDateString(),
          updatedAt: new Date().toDateString()
        },
        {
          user_id: 2,
          task_id: 2,
          following: 0,
          createdAt: new Date().toDateString(),
          updatedAt: new Date().toDateString()
        },
        {
          user_id: 3,
          task_id: 2,
          following: 0,
          createdAt: new Date().toDateString(),
          updatedAt: new Date().toDateString()
        },
      ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
