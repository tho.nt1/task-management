const { request } = require('express');
const constant = require('../constant');
const models = require('../models'); 

exports.index = async function() {
    const users = await models.User.findAll()
    .then((users) => {
        return users;
    })
    .catch((err) => {
        throw err;
    });

    return users;
};

exports.deleteAllDataOfUser = async function(query) {
    const { uuid } = query;
    let condition = {};
    if (!uuid) {
        return {
            code: constant.CODE_ERR,
            msg: "Please choose user to delete",
            data: null
        };
    }
    const user = await models.User.findOne({ where: { uuid }});
    if (!user) {
        return {
            code: constant.CODE_ERR,
            msg: "User not exit",
            data: null
        }
    }

    const tasks = await models.Task.findAll({ where: {user_id: user.id }});
    const taskIds = tasks.reduce((values, current) => {
        values.push(current.id);
        return values;
    }, []);

    await models.Task.destroy({ where: {user_id: user.id }});
    await models.TaskUser.destroy({where: {user_id: user.id}});
    await models.TaskUser.destroy({where: {task_id: taskIds}});
    return {
        code: constant.CODE_SUCCESS,
        msg: `Delete all data of user that has uuid=${uuid}`,
        user,
    }
};


exports.searchMarkTasks = async function(query) {
    const { uuid_assignee, status } = query;
    console.log(uuid_assignee, status);
    if (!uuid_assignee || status != constant.STATUS_COMPLETED && status != constant.STATUS_IN_COMPLETE) {
        return {
            code: constant.CODE_ERR,
            msg: "Please fill uuid and statis is: 'completed' or 'in_complete'",
            data: null
        }
    }

    const assignee = await models.User.findOne({ where: { uuid: uuid_assignee }});
    if (!assignee) {
        return {
            code: constant.CODE_ERR,
            msg: "Assignee not exit",
            data: null
        }
    }

    const tasks = await models.Task.findAll({ where: {assignee: assignee.id, status}});
    return {
        code: constant.CODE_SUCCESS,
        msg: "search task successful",
        data: tasks
    };
};

exports.searchFollowingTasks = async function(query) {
    const { uuid } = query;
    if (!uuid) {
        return {
            code: constant.CODE_ERR,
            msg: "Please fill uuid",
            data: null
        }
    }

    const user = await models.User.findOne({ where: { uuid }});
    if (!user) {
        return {
            code: constant.CODE_ERR,
            msg: "User not exit",
            data: null
        }
    }

    const taskUsers = await models.TaskUser.findAll({
        attributes: ['task_id'],
        where: {user_id: user.id}
    });

    const taskIds = taskUsers.reduce((values, current) => {
        values.push(current.task_id);
        return values;
    }, []);

    const tasks = await models.Task.findAll({
        where: {
            id: taskIds
          }
    });

    return {
        code: constant.CODE_SUCCESS,
        msg: "search following task successful",
        data: tasks
    };
};
