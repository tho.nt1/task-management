const models = require('../models'); 
const constant = require('../constant');

exports.index = async function() {
    const users = await models.Admin.findAll()
    .then((users) => {
        return users;
    })
    .catch((err) => {
        throw err;
    });

    return {
        code: constant.CODE_SUCCESS,
        msg: "Get users success",
        data: users
    }
};

exports.getTasks = async function(query) {
    const { uuid } = query;
    let condition = {};
    if (uuid) {
        const user = await models.User.findOne({ where: { uuid }});
        condition = { where: {user_id: user.id}};
    }
    const tasks = await models.Task.findAll(condition);
    return {
        code: constant.CODE_SUCCESS,
        msg: "Get tasks success",
        data: tasks
    }
};
