const models = require('../models'); 
const constant = require('../constant');

/**
 * Create new task
 * @param {object} query: contain data for create task, in this case it contain:
 *  - uuid {String}: the uuid of user that create this task
 *  - uuid_assignee: the uuid of assignee user, who will do this task
 *  - content: the content of this task 
 * @returns 
 *  - if success: return code, message success and the new task (that has been just created)
 *  - if fail: return code, message fail
 */
exports.create = async function(query) {
    if (!query) {
        return {
            code: constant.CODE_ERR_MISSING_INPUT,
            msg: "Please fill uuid, uuid_assignee (uuid of assignee) and have content",
            data: null
        }
    }
    const { uuid, uuid_assignee, content } = query;
    if (!uuid || !uuid_assignee || !content) {
        return {
            code: constant.CODE_ERR_MISSING_INPUT,
            msg: "Please fill uuid, uuid_assignee (uuid of assignee) and have content",
            data: null
        }
    }
    const user = await models.User.findOne({ where: { uuid }});
    const assignee = await models.User.findOne({ where: { uuid: uuid_assignee}});
    if (!user || !assignee) {
        return {
            code: constant.CODE_ERR_NOT_EXIST_DATA,
            msg: "User or Assignee not exit",
            data: null
        }
    }

    const status = constant.STATUS_IN_COMPLETE;
    const task = await models.Task.create({
        user_id :user.id,
        assignee: assignee.id,
        status,
        content,
    })
    return {
        code: constant.CODE_SUCCESS,
        msg: "Create task successfull",
        data: task
    };
};

/**
 * Update existed task, for thsi case, we just update content
 * @param {integer} task_id: the id of task we need to update 
 * @param {object} query: contain data for update existed task, in this case it contain:
 *  - content: the updated content of this task 
 * @returns 
 *  - if success: return code, message success and the new task (that has been just created)
 *  - if fail: return code, message fail
 */
exports.update = async function(task_id, query) {
    if (!task_id) {
        return {
            code: constant.CODE_ERR_MISSING_INPUT,
            msg: "Please fill task_id",
            data: null
        };
    }

    const task = await models.Task.findByPk(task_id);
    if (!task) {
        return {
            code: constant.CODE_ERR_NOT_EXIST_DATA,
            msg: "Task not exit",
            data: null
        };
    }

    if (!query || !query.content) {
        return {
            code: constant.CODE_ERR_MISSING_INPUT,
            msg: "Nothing need to update",
            task:null
        };
    }
    await task.update({content: query.content});
    return {
        code: constant.CODE_SUCCESS,
        msg: "Updated task successfull",
        data: task
    };
};

exports.delete = async function(task_id) {
    if (!task_id) {
        return {
            code: constant.CODE_ERR,
            msg: "Please fill task_id",
            data: null
        };
    }

    const task = await models.Task.findByPk(task_id);
    if (!task) {
        return {
            code: constant.CODE_ERR,
            msg: "Task not exit",
            data: null
        };
    }

    await task.destroy();
    return {
        code: constant.CODE_SUCCESS,
        msg: "Updated task successfull",
        data: task
    };
};

exports.mark = async function(task_id, query) {
    const { uuid_assignee, status } = query;
    if (!task_id || !uuid_assignee || !status) {
        return {
            code: constant.CODE_ERR,
            msg: "Please fill task_id, uuid_assignee and status",
            data: null
        };
    }
    const task = await models.Task.findByPk(task_id);
    if (!task) {
        return {
            code: constant.CODE_ERR,
            msg: "Task not exit",
            data: null
        };
    }
    const assignee = await models.User.findOne({ where: { uuid: uuid_assignee}});
    if (!assignee) {
        return {
            code: constant.CODE_ERR,
            msg: "Assignee not exit",
            data: null
        }
    }
    if (assignee.id != task.assignee) {
        return {
            code: constant.CODE_ERR,
            msg: "You are not assignee of this task, so you cannot mark or unmark it.",
            data: null
        }
    }

    if (status != constant.STATUS_COMPLETED && status != constant.STATUS_IN_COMPLETE) {
        return {
            code: constant.CODE_ERR,
            msg: "Please fill statis is: 'completed' or 'in_complete'",
            data: null
        }
    }
    await task.update({status});
    return {
        code: constant.CODE_SUCCESS,
        msg: `Your mark ${status == constant.STATUS_COMPLETED ? 'completed' : 'in_complete'}`,
        data: task,
    }
};

exports.following = async function(task_id, query) {
    const { uuid } = query;
    if (!task_id || !uuid) {
        return {
            code: constant.CODE_ERR,
            msg: "Please fill task_id, uuid",
            data: null
        };
    }
    const task = await models.Task.findByPk(task_id);
    if (!task) {
        return {
            code: constant.CODE_ERR,
            msg: "Task not exit",
            data: null
        };
    }
    const user = await models.User.findOne({ where: { uuid }});
    if (!user) {
        return {
            code: constant.CODE_ERR,
            msg: "User not exit",
            data: null
        }
    }

    let following = await models.TaskUser.findOne({ where: { user_id: user.id, task_id }});
    let msg = '';
    if (following) {
        msg = 'Unfollowing successfull';
        await following.destroy();
    } else {
        msg = 'Following successfull';
        following = await models.TaskUser.create({ user_id: user.id, task_id });
    }
    return {
        code: constant.CODE_SUCCESS,
        msg,
        data:following
    }
};
