const task_service = require('../services/taskService');

exports.create = async function(req, res) {
    try {
        const result = await task_service.create(req.query);
        res.send(result);
    } catch (e) {
        console.log(e);
        res.send('There are something wrong!')
    }
};

exports.update = async function(req, res) {
    try {
        const task_id = req.params.id;
        const result = await task_service.update(task_id, req.query);
        res.send(result);
    } catch (e) {
        console.log(e);
        res.send('There are something wrong!')
    }
};

exports.delete = async function(req, res) {
    try {
        const task_id = req.params.id;
        const result = await task_service.delete(task_id);
        res.send(result);
    } catch (e) {
        console.log(e);
        res.send('There are something wrong!')
    }
};

exports.mark = async function(req, res) {
    try {
        const task_id = req.params.id;
        const result = await task_service.mark(task_id, req.query);
        res.send(result);
    } catch (e) {
        console.log(e);
        res.send('There are something wrong!')
    }
};

exports.following = async function(req, res) {
    try {
        const task_id = req.params.id;
        const result = await task_service.following(task_id, req.query);
        res.send(result);
    } catch (e) {
        console.log(e);
        res.send('There are something wrong!')
    }
};
