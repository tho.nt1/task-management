const user_service = require('../services/userService');

// exports.index = async function(req, res) {
//     try {
//         const users = await user_service.index();
//         res.send(users)
//     } catch (e) {
//         console.log(e);
//         res.send('There are something wrong!')
//     }
// };

exports.deleteAllDataOfUser = async function(req, res) {
    try {
        const users = await user_service.deleteAllDataOfUser(req.query);
        res.send(users)
    } catch (e) {
        console.log(e);
        res.send('There are something wrong!')
    }
};

exports.searchMarkTasks = async function(req, res) {
    try {
        console.log(req);
        const result = await user_service.searchMarkTasks(req.query);
        res.send(result);
    } catch (e) {
        console.log(e);
        res.send('There are something wrong!')
    }
};

exports.searchFollowingTasks = async function(req, res) {
    try {
        const result = await user_service.searchFollowingTasks(req.query);
        res.send(result);
    } catch (e) {
        console.log(e);
        res.send('There are something wrong!')
    }
};
