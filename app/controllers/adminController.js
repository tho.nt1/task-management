const admin_service = require('../services/adminService');

exports.index = async function(req, res) {
    try {
        const admins = await admin_service.index();
        res.send(admins)
    } catch (e) {
        console.log(e);
        res.send('There are something wrong!')
    }
};

exports.getTasks = async function(req, res) {
    try {
        const admins = await admin_service.getTasks(req.query);
        res.send(admins)
    } catch (e) {
        console.log(e);
        res.send('There are something wrong!')
    }
};