const expect    = require("chai").expect;
const task_service = require('../services/taskService');
const constant = require('../constant');
const data_test = require('./data');

let created_task_id = null;

describe("Creating task", function() {
    describe("Error: missing input data", function() {
        it("Missing all", async function() {
            const result = await task_service.create();
            expect(result.code).to.equal(constant.CODE_ERR_MISSING_INPUT);
        });
        it("Missing uuid", async function() {
            const result = await task_service.create({
                uuid_assignee: data_test.USER_TEST.user2.uuid,
                content: 'test content task 1',
            });
            expect(result.code).to.equal(constant.CODE_ERR_MISSING_INPUT);
        });
        it("Missing uuid_assignee", async function() {
            const result = await task_service.create({
                uuid: data_test.USER_TEST.user1.uuid,
                content: 'test content task 1',
            });
            expect(result.code).to.equal(constant.CODE_ERR_MISSING_INPUT);
        });
        it("Missing content", async function() {
            const result = await task_service.create({
                uuid: data_test.USER_TEST.user1.uuid,
                uuid_assignee: data_test.USER_TEST.user2.uuid,
            });
            expect(result.code).to.equal(constant.CODE_ERR_MISSING_INPUT);
        });
    });

    describe("Error: data is wrong", function() {
        it("Uuid of creator not exist in User table", async function() {
            const result = await task_service.create({
                uuid: 'user_not_exist',
                uuid_assignee: data_test.USER_TEST.user2.uuid,
                content: 'test content task 1',
            });
            expect(result.code).to.equal(constant.CODE_ERR_NOT_EXIST_DATA);
        });
        it("Uuid of assignee not exist in User table", async function() {
            const result = await task_service.create({
                uuid: data_test.USER_TEST.user1.uuid,
                uuid_assignee: 'user_not_exist',
                content: 'test content task 1',
            });
            expect(result.code).to.equal(constant.CODE_ERR_NOT_EXIST_DATA);
        });
    });
    describe("Creating task successful", function() {
        it("Creating task successfull", async function() {
            const data = {
                uuid: data_test.USER_TEST.user1.uuid,
                uuid_assignee: data_test.USER_TEST.user2.uuid,
                content: 'test content task 1',
            };
            const result = await task_service.create(data);

            expect(result.code).to.equal(constant.CODE_SUCCESS);
            expect(result.data.user_id).to.equal(data_test.USER_TEST.user1.id);
            expect(result.data.assignee).to.equal(data_test.USER_TEST.user2.id);
            expect(result.data.content).to.equal(data.content);
            created_task_id = result.data.id;

        });
    });
});

describe("Update exist task", function() {
    describe("Error: missing input data", function() {
        it("Missing all", async function() {
            const result = await task_service.update();
            expect(result.code).to.equal(constant.CODE_ERR_MISSING_INPUT);
        });
        it("Missing task_id", async function() {
            const result = await task_service.update(null, {
                content: 'test update content task 1',
            });
            expect(result.code).to.equal(constant.CODE_ERR_MISSING_INPUT);
        });
        it("Missing content", async function() {
            const result = await task_service.update(created_task_id);
            expect(result.code).to.equal(constant.CODE_ERR_MISSING_INPUT);
        });
    });

    describe("Error: data is wrong", function() {
        it("Updated task not exist", async function() {
            const task_id_not_exist = -1;
            const result = await task_service.update(task_id_not_exist, {
                content: 'test update content task 1',
            });
            expect(result.code).to.equal(constant.CODE_ERR_NOT_EXIST_DATA);
        });
    });
    describe("Updating task successful", function() {
        it("Creating task successfull", async function() {
            const data = {
                content: 'test content task 1',
            };
            const result = await task_service.update(created_task_id ,data);

            expect(result.code).to.equal(constant.CODE_SUCCESS);
            expect(result.data.id).to.equal(created_task_id);
            expect(result.data.content).to.equal(data.content);
            console.log(result.data);
        });
    });
});
