exports.USER_TEST = {
    user1: {
        id: 1,
        uuid: 'user_1',
    },
    user2: {
        id: 2,
        uuid: 'user_2',
    },
    user3: {
        id: 3,
        uuid: 'user_3',
    },
    user4: {
        id: 4,
        uuid: 'user_4',
    },
    user5: {
        id: 5,
        uuid: 'user_5',
    },
}