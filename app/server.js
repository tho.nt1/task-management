// const bodyParser = require('body-parser');
const express = require('express')
const rootRouter = require('./routes/index');
const app = express()
const swaggerUi = require('swagger-ui-express');
const swagger = require('./swagger');

const port = process.env.PORT | 3000

const openapiSpecification = swagger.openapiSpecification();
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(openapiSpecification));

app.use(rootRouter);

app.listen(port, () => {
  console.log(`We listion on http://localhost:${port}`)
});
